/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.singleton.app.arquitectura.tp.models;

/**
 *
 * @author pato
 */
public class Memories {
    private long value;
    private String reference;
    
    public Memories() {
    }

    public Memories(long value, String reference) {
        this.value = value;
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
    
    public String getHexa(){
        return Long.toHexString(value);
    }
    
    public String getBinary(){
        return Long.toBinaryString(value);
    }
}
